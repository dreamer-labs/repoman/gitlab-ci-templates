import sys
import pytest
import os

pth = os.path.realpath(os.path.join(__file__, '..', '..', 'tools'))
sys.path.append(pth)
import image_snatch  # noqa E402


@pytest.mark.parametrize('params', [
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--resolve-linked',
     '--list-images',
     '--list-includes'],
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--resolve-linked',
     '--list-images']
])
def test_usage_images(capsys, params):
    image_snatch.main(params)
    captured = capsys.readouterr()

    assert ('registry.gitlab.com/dreamer-labs/repoman/dl-commitlint:latest'
            in captured.out)

    assert ('pipelinecomponents/flake8'
            in captured.out)


@pytest.mark.parametrize('params', [
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--resolve-linked',
     '--list-images',
     '--list-includes'],
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--resolve-linked',
     '--list-includes'],
])
def test_usage_includes(capsys, params):
    image_snatch.main(params)
    captured = capsys.readouterr()
    assert '.test-nested.yml' in captured.out
    assert '.nested.yml' in captured.out
    assert '.test-commitchecker.yml' in captured.out


@pytest.mark.parametrize('params', [
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--list-images',
     '--list-includes'],
    ['tests/fixtures/.test-gitlab-ci.yml',
     '--list-includes'],
])
def test_usage_includes_no_linked(capsys, params):
    image_snatch.main(params)
    captured = capsys.readouterr()

    assert '.test-nested.yml' in captured.out
    assert '.test-commitchecker.yml' in captured.out
    assert '.nested.yml' not in captured.out


@pytest.mark.parametrize('params', [
    ['tests/fixtures/.gitlab-ci-recurse.yml',
     '--resolve-linked',
     '--list-images',
     '--list-includes'],
    ['tests/fixtures/.gitlab-ci-recurse.yml',
     '--resolve-linked',
     '--list-includes'],
])
def test_usage_includes_recursion(params):

    with pytest.raises(Exception) as except_err:
        image_snatch.main(params)

    assert 'Recursion Detected' in str(except_err.value)

#!/bin/env python3
import sys
import json
import yaml
import argparse
from os import path, chdir, getcwd

from urllib.parse import urlparse
from urllib.request import Request, urlopen

import contextlib


@contextlib.contextmanager
def remember_cwd():
    curdir = getcwd()
    try:
        yield
    finally:
        chdir(curdir)


SEEN = []


def get_include(include, follow_includes=False):  # noqa: C901
    global SEEN

    ci_data = {}

    if isinstance(include, str):
        if include in SEEN:
            raise Exception(f'Recursion Detected for {include}')
        SEEN.append(include)
        uri = urlparse(include)
        if uri.scheme == '' or uri.scheme == 'file':
            try:
                file_path = uri.path
                if file_path[0] == path.sep:
                    file_path = file_path.lstrip(path.sep)

                with open(file_path, 'r') as ci_file:
                    contents = yaml.safe_load(ci_file)

                ci_data[file_path] = contents

                if follow_includes and 'include' in contents:
                    ci_data.update(get_include(contents['include'],
                                               follow_includes))
                return ci_data
            except FileNotFoundError:
                pass

        elif uri.scheme in ('https', 'http'):
            req = Request(uri.geturl(), headers={
                'User-Agent': 'cloudflare is agentist'
            })
            contents = urlopen(req)
            data = contents.read()
            if not data.find('<html'.encode()) < 0:
                raise Exception('Recieved HTML instead of a yaml file')
            return {uri.geturl(): yaml.safe_load(data)}
        else:
            raise NotImplementedError(f'Unsupported uri scheme {uri.scheme}')
    elif isinstance(include, dict):

        for k, v in include.items():
            ci_data.update(get_include(v, follow_includes))
        return ci_data
    elif isinstance(include, list):

        for v in include:
            ci_data.update(get_include(v, follow_includes))
        return ci_data
    else:
        raise NotImplementedError(f'Unsupported include type {type(include)}')


def get_image(image):
    if isinstance(image, dict):
        return image['name']
    else:
        return image


def gitlab_ci_images(ci_things):
    images = []

    for include_file, data in ci_things.items():
        for k, v in data.items():
            if 'image' in v:
                images.append(get_image(v['image']))

    return images


def gitlab_ci_includes(ci_things):
    includes = []

    for include_file, data in ci_things.items():
        for k, v in data.items():
            if k == 'include':
                if isinstance(v, list):
                    includes.extend(v)
                else:
                    includes.append(v)

    return includes


def main(argv):
    global SEEN

    SEEN.clear()
    parser = argparse.ArgumentParser('image-snatch')
    parser.add_argument('ci_uri',
                        help='root gitlab-ci.yml file',
                        default='.gitlab-ci.yml')

    parser.add_argument('--resolve-linked',
                        action='store_true')

    parser.add_argument('--list-images',
                        action='store_true')

    parser.add_argument('--list-includes',
                        action='store_true')

    opts = parser.parse_args(argv)
    if not opts.list_images and not opts.list_includes:
        opts.list_images = True

    base_ci = urlparse(opts.ci_uri)
    if base_ci.scheme not in ('https', 'http'):
        base_path = path.dirname(path.realpath(base_ci.path))
    else:
        base_path = ''
    with remember_cwd():
        if base_path:
            chdir(base_path)
            ci_uri = path.basename(opts.ci_uri)
        else:
            ci_uri = opts.ci_uri

        ci_things = get_include(ci_uri, follow_includes=opts.resolve_linked)

    output = {}
    if opts.list_images:
        output['images'] = gitlab_ci_images(ci_things)

    if opts.list_includes:
        output['includes'] = gitlab_ci_includes(ci_things)

    print(json.dumps(output))


if __name__ == '__main__':
    main(sys.argv[1:])

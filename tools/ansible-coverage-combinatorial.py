#!/bin/env python3

import argparse
import json
import re


def filter_tasks(filter_by, task):
    matched = False

    for filter_re in filter_by:
        if matched:
            break
        matched = re.search(filter_re, task) is not None

    return matched


def was_task_run(states):
    run_states = ('ok', 'failed')
    return len(set(run_states).intersection(set(states))) > 0


def main():

    args = argparse.ArgumentParser('ansible-coverage-combinatorial')

    args.add_argument('coverage_files',
                      nargs='*')

    args.add_argument('--coverage',
                      action='store_true',
                      help='Calculate the percent coverage for matched tasks')

    args.add_argument('--show-missing',
                      action='store_true',
                      help='Show report of tasks missing coverage')

    args.add_argument('--show-covered',
                      action='store_true',
                      help='Show report of tasks missing coverage')

    args.add_argument('--outfile',
                      default=None)

    args.add_argument('--filter-by',
                      nargs='*',
                      default=[],
                      help='A space separated list of regex'
                           ' patterns to filter tasks by')

    opts = args.parse_args()
    combined = {}
    for cov_file in opts.coverage_files:
        with open(cov_file) as cov:
            combined.update(json.load(cov))

    if opts.outfile:
        with open(opts.outfile, 'w') as output:
            json.dump(combined, output)

    if opts.coverage:
        tasks = {}
        for scenario, runs in combined.items():
            for run in runs:
                for task in run['tasks']:
                    if not len(opts.filter_by) or filter_tasks(
                            opts.filter_by, task[0]):
                        tasks.setdefault(task[0], [])
                        tasks[task[0]].append(task[1])

        total_tasks = len(tasks)
        ok_tasks = 0

        for task, states in tasks.items():
            if was_task_run(states):
                ok_tasks += 1

        if opts.show_missing:
            for task, states in tasks.items():
                if not was_task_run(states):
                    task_title = task[:80]
                    padding = ' ' * (85 - len(task_title))
                    print(f'{task_title}:{padding} {",".join(set(states))}')

        if opts.show_covered:
            for task, states in tasks.items():
                if was_task_run(states):
                    task_title = task[:80]
                    padding = ' ' * (85 - len(task_title))
                    print(f'{task_title}:{padding} {",".join(set(states))}')

        if total_tasks == 0:
            print(f'total coverage: {0}')
        else:
            total_coverage = (ok_tasks / total_tasks) * 100
            print(f'total_coverage: {total_coverage}')


if __name__ == '__main__':
    main()
